﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projekat.Models
{
    public enum tip_karte { VIP, REGULAR, FAN_PIT};
    public enum status_karte { REZERVISANA, ODUSTANAK};

    public class Karta
    {
        private string id;
        private Manifestacija manifestacija;
        private DateTime dateTime;
        private double cena;
        private string kupac;
        private tip_karte tk;
        private status_karte sk;

        public Karta(string id, Manifestacija manifestacija, DateTime dateTime, double cena, string kupac, tip_karte tk, status_karte sk)
        {
            this.id = id;
            this.manifestacija = manifestacija;
            this.dateTime = dateTime;
            this.cena = cena;
            this.kupac = kupac;
            this.tk = tk;
            this.sk = sk;
        }

        public string Id { get => id; set => id = value; }
        public Manifestacija Manifestacija { get => manifestacija; set => manifestacija = value; }
        public DateTime DateTime { get => dateTime; set => dateTime = value; }
        public double Cena { get => cena; set => cena = value; }
        public string Kupac { get => kupac; set => kupac = value; }
        public tip_karte Tk { get => tk; set => tk = value; }
        public status_karte Sk { get => sk; set => sk = value; }
    }
}