﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projekat.Models
{
    public class Rezervacija
    {
        private string id;
        private string ime_manifestacije;
        //private tip_karte tipKarte;
        private string kupac;

        public Rezervacija(string id, string ime_manifestacije, string kupac)
        {
            this.Id = id;
            this.Ime_manifestacije = ime_manifestacije;
            //this.TipKarte = tipKarte;
            this.Kupac = kupac;
        }

        public string Id { get => id; set => id = value; }
        public string Ime_manifestacije { get => ime_manifestacije; set => ime_manifestacije = value; }
        //public tip_karte TipKarte { get => tipKarte; set => tipKarte = value; }
        public string Kupac { get => kupac; set => kupac = value; }
    }
}