﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace Projekat.Models
{
    public class Komentari
    {
        public List<Komentar> SpisakKomentara { get; set; }

        public Komentari(string putanja)
        {
            SpisakKomentara = new List<Komentar>();
            putanja = HostingEnvironment.MapPath(putanja);
            FileStream stream = new FileStream(putanja, FileMode.Open);
            StreamReader sr = new StreamReader(stream);
            string linija = "";
            while ((linija = sr.ReadLine()) != null)
            {
                string[] parametri = linija.Split('|');


                Komentar k = new Komentar(parametri[1], parametri[0], parametri[2], int.Parse(parametri[3]));
                SpisakKomentara.Add(k);
            }
            sr.Close();
            stream.Close();
        }

        public void UpisiKomentare()
        {
            string putanja = HostingEnvironment.MapPath("~/App_Data/Komentari.txt");
            FileStream stream = new FileStream(putanja, FileMode.Create);
            StreamWriter sw = new StreamWriter(stream);
            foreach (Komentar k in SpisakKomentara)
            {

                //if (k.Logicki_obrisan == true) { obrisan = "da"; } else { obrisan = "ne"; }
                sw.WriteLine(k.Manifestacija + "|" + k.Korisnicko_ime + "|" + k.Tekst_komentara + "|" + k.Ocena.ToString());
            }
            sw.Close();
            stream.Close();
        }
    }
}