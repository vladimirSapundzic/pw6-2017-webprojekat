﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projekat.Models
{
    public class MestoOdrzavanja
    {
        private string ulica;
        private string mesto;
        private long postanski_broj;

        public MestoOdrzavanja(string ulica, string mesto, long postanski_broj)
        {
            this.Ulica = ulica;
            this.Mesto = mesto;
            this.Postanski_broj = postanski_broj;
        }

        public string Ulica { get => ulica; set => ulica = value; }
        public string Mesto { get => mesto; set => mesto = value; }
        public long Postanski_broj { get => postanski_broj; set => postanski_broj = value; }
    }
}