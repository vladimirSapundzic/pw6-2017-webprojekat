﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projekat.Models
{
    public class ManifestacijaParam
    {
        public string naziv { get; set; }
        public string tip { get; set; }
        public int broj_mesta { get; set; }
        public string datum_odrzavanja { get; set; }
        public double cena_karte { get; set; }
        public string ulica { get; set; }
        public string mesto { get; set; }
        public long postanski_broj { get; set; }

        public ManifestacijaParam() { }

        public ManifestacijaParam(string naziv, string tip, int broj_mesta, string vreme_odrzavanja, double cena_karte, string ulica, string mesto, long postanski_broj)
        {
            this.naziv = naziv;
            this.tip = tip;
            this.broj_mesta = broj_mesta;
            this.datum_odrzavanja = vreme_odrzavanja;
            this.cena_karte = cena_karte;
            this.ulica = ulica;
            this.mesto = mesto;
            this.postanski_broj = postanski_broj;
        }
    }
}