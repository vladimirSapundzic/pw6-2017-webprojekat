﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace Projekat.Models
{
    public class RezervisaneKarte
    {
        public List<Rezervacija> SpisakRezervacija { get; set; }

        public RezervisaneKarte(string putanja)
        {
            SpisakRezervacija = new List<Rezervacija>();
            putanja = HostingEnvironment.MapPath(putanja);
            FileStream stream = new FileStream(putanja, FileMode.Open);
            StreamReader sr = new StreamReader(stream);
            string linija = "";
            while ((linija = sr.ReadLine()) != null)
            {
                string[] parametri = linija.Split('|');
                


                Rezervacija r = new Rezervacija(parametri[0], parametri[1], parametri[2]);
                SpisakRezervacija.Add(r);
            }
            sr.Close();
            stream.Close();
        }

        public void UpisiRezervacije()
        {
            string putanja = HostingEnvironment.MapPath("~/App_Data/RezervisaneKarte.txt");
            FileStream stream = new FileStream(putanja, FileMode.Create);
            StreamWriter sw = new StreamWriter(stream);
            foreach (Rezervacija r in SpisakRezervacija)
            {

                //if (k.Logicki_obrisan == true) { obrisan = "da"; } else { obrisan = "ne"; }

                sw.WriteLine(r.Id + "|" + r.Ime_manifestacije + "|" + r.Kupac);
            }
            sw.Close();
            stream.Close();
        }
    }
}