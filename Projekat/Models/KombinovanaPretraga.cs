﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projekat.Models
{
    public class KombinovanaPretraga
    {
        public string tip_kar { get; set; }
        public string tip_man { get; set; }
        public double cena_od { get; set; }
        public double cena_do { get; set; }
        public string datum_od { get; set; }
        public string datum_do { get; set; }

        public KombinovanaPretraga() { }
    }
}