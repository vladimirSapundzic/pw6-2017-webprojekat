﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;

namespace Projekat.Models
{
    public enum tip_manifestacije {KONCERT, FESTIVAL, POZORISTE}
    public enum status {AKTIVNO, NEAKTIVNO}

    public class Manifestacija
    {
        private string naziv;
        private tip_manifestacije tip_man;
        private int broj_mesta;
        private DateTime vreme_odr;
        private double cena_karte;
        private status st;
        private MestoOdrzavanja mestoOdrzavanja;
        //private Bitmap slika;
        public string prodavac { get; set; }

        public Manifestacija(string naziv, tip_manifestacije tip_man, int broj_mesta, DateTime vreme_odr, double cena_karte, status st, MestoOdrzavanja mestoOdrzavanja, string prodavac)
        {
            this.Naziv = naziv;
            this.Tip_man = tip_man;
            this.Broj_mesta = broj_mesta;
            this.Vreme_odr = vreme_odr;
            this.Cena_karte = cena_karte;
            this.St = st;
            this.MestoOdrzavanja = mestoOdrzavanja;
            this.prodavac = prodavac;
            //this.Slika = slika;
        }
        public Manifestacija() { }

        public string Naziv { get => naziv; set => naziv = value; }
        public tip_manifestacije Tip_man { get => tip_man; set => tip_man = value; }
        public int Broj_mesta { get => broj_mesta; set => broj_mesta = value; }
        public DateTime Vreme_odr { get => vreme_odr; set => vreme_odr = value; }
        public double Cena_karte { get => cena_karte; set => cena_karte = value; }
        public status St { get => st; set => st = value; }
        public MestoOdrzavanja MestoOdrzavanja { get => mestoOdrzavanja; set => mestoOdrzavanja = value; }
        //public Bitmap Slika { get => slika; set => slika = value; }
    }
}