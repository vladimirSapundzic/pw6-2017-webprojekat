﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projekat.Models
{
    public enum Pol_korisnika {M, Z};
    public enum Uloga_korisnika {KUPAC, PRODAVAC, ADMINISTRATOR};

    public class Korisnik
    {
        public Korisnik()
        {
        }

        public Korisnik(string username, string lozinka, string ime, string prezime, Pol_korisnika pol, DateTime datum_rodjenja, Uloga_korisnika uloga, int bodovi, bool log_obrisan)
        {
            Username = username;
            Lozinka = lozinka;
            Ime = ime;
            Prezime = prezime;
            Pol = pol;
            Datum_rodjenja = datum_rodjenja;
            Uloga = uloga;
            Bodovi = bodovi;
            Logicki_obrisan = log_obrisan;

            Tipovi_korisnika tk = new Tipovi_korisnika();
            if (bodovi < 1000)
            {
                Tip = tk.tipovi[0];
            }
            else if (bodovi >= 1000 && bodovi < 2000)
            {
                Tip = tk.tipovi[1];
            }
            else {
                Tip = tk.tipovi[2];
            }
        }

        public string Username { get; set; }
        public string Lozinka { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public Pol_korisnika Pol { get; set; }
        public DateTime Datum_rodjenja { get; set; }
        public Uloga_korisnika Uloga { get; set; }
        //SveKarteBezObziraNaStatus(ako je korisnik Kupac)
        //Manifestacije(ako je korisnik Prodavac)
        public int Bodovi { get; set; }
        public Tip_korisnika Tip { get; set; }
        public bool Logicki_obrisan { get; set; }

    }
}