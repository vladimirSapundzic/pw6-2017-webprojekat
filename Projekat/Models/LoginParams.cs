﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projekat.Models
{
    public class LoginParams
    {
        private string username;
        private string lozinka;

        public string Username { get => username; set => username = value; }
        public string Lozinka { get => lozinka; set => lozinka = value; }

        public LoginParams(string username, string lozinka)
        {
            this.Username = username;
            this.Lozinka = lozinka;
        }

        public LoginParams() { }
    }
}