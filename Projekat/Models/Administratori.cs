﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace Projekat.Models
{
    public class Administratori
    {
        public List<Korisnik> SpisakAdministratora { get; set; }

        public Administratori(string putanja)
        {
            SpisakAdministratora = new List<Korisnik>();
            putanja = HostingEnvironment.MapPath(putanja);
            FileStream stream = new FileStream(putanja, FileMode.Open);
            StreamReader sr = new StreamReader(stream);
            string linija = "";
            while ((linija = sr.ReadLine()) != null)
            {
                string[] parametri = linija.Split(',');
                

                Korisnik k = new Korisnik(parametri[0], parametri[1], "", "",  Pol_korisnika.M,
                                                   DateTime.Now, Uloga_korisnika.ADMINISTRATOR, 0, false);
                SpisakAdministratora.Add(k);
            }
            sr.Close();
            stream.Close();
        }

        public void UpisiAdministratore()
        {
            string putanja = HostingEnvironment.MapPath("~/App_Data/Administratori.txt");
            FileStream stream = new FileStream(putanja, FileMode.Create);
            StreamWriter sw = new StreamWriter(stream);
            foreach (Korisnik k in SpisakAdministratora)
            {

                //if (k.Logicki_obrisan == true) { obrisan = "da"; } else { obrisan = "ne"; }
                sw.WriteLine(k.Username + "," + k.Lozinka);
            }
            sw.Close();
            stream.Close();
        }
    }
}