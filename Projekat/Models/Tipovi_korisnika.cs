﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projekat.Models
{
    public class Tipovi_korisnika
    {
        public List<Tip_korisnika> tipovi;

        public Tipovi_korisnika()
        {
            this.tipovi = new List<Tip_korisnika>();
            Tip_korisnika Bronzani = new Tip_korisnika("Bronzani", 3, 1000);
            Tip_korisnika Srebrni = new Tip_korisnika("Srebrni", 5, 2000);
            Tip_korisnika Zlatni = new Tip_korisnika("Zlatni", 10, 3000);
            tipovi.Add(Bronzani);
            tipovi.Add(Srebrni);
            tipovi.Add(Zlatni);

        }
    }
}