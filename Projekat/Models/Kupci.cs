﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace Projekat.Models
{
    public class Kupci
    {
        public List<Korisnik> SpisakKupaca { get; set; }

        public Kupci(string putanja)
        {
            SpisakKupaca = new List<Korisnik>();
            putanja = HostingEnvironment.MapPath(putanja);
            FileStream stream = new FileStream(putanja, FileMode.Open);
            StreamReader sr = new StreamReader(stream);
            string linija = "";
            while ((linija = sr.ReadLine()) != null)
            {
                string[] parametri = linija.Split(',');
                Pol_korisnika pol_kupca;
                if (string.Equals(parametri[4], "M")) { pol_kupca = Pol_korisnika.M; } else { pol_kupca = Pol_korisnika.Z; }

                Korisnik k = new Korisnik(parametri[0], parametri[1], parametri[2], parametri[3], pol_kupca,
                                                   DateTime.Parse(parametri[5]), Uloga_korisnika.KUPAC, int.Parse(parametri[6]), bool.Parse(parametri[7]));
                SpisakKupaca.Add(k);
            }
            sr.Close();
            stream.Close();
        }

        public void UpisiKupce()
        {
            string putanja = HostingEnvironment.MapPath("~/App_Data/Kupci.txt");
            FileStream stream = new FileStream(putanja, FileMode.Create);
            StreamWriter sw = new StreamWriter(stream);
            foreach (Korisnik k in SpisakKupaca)
            {

                //if (k.Logicki_obrisan == true) { obrisan = "da"; } else { obrisan = "ne"; }
                sw.WriteLine(k.Username + "," + k.Lozinka + "," + k.Ime + "," + k.Prezime + "," +
                             k.Pol.ToString() + "," + k.Datum_rodjenja.ToShortDateString() + "," + k.Bodovi.ToString() + "," + k.Logicki_obrisan.ToString());
            }
            sw.Close();
            stream.Close();
        }
    }
}