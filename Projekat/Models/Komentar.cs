﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projekat.Models
{
    public class Komentar
    {
        private string korisnicko_ime;
        private string manifestacija;

        private string tekst_komentara;
        private int ocena;

        public Komentar(string korisnicko_ime, string manifestacija, string tekst_komentara, int ocena)
        {
            Korisnicko_ime = korisnicko_ime;
            Manifestacija = manifestacija;
            Tekst_komentara = tekst_komentara;
            Ocena = ocena;
        }

        public Komentar()
        {
            Korisnicko_ime = "";
            Manifestacija = "";
            Tekst_komentara = "";
            Ocena = 0;
        }

        public string Korisnicko_ime { get => korisnicko_ime; set => korisnicko_ime = value; }
        public string Manifestacija { get => manifestacija; set => manifestacija = value; }
        public string Tekst_komentara { get => tekst_komentara; set => tekst_komentara = value; }
        public int Ocena { get => ocena; set => ocena = value; }
    }
}