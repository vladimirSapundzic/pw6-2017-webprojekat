﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace Projekat.Models
{
    public class Prodavci
    {
        public List<Korisnik> SpisakProdavaca { get; set; }

        public Prodavci(string putanja)
        {
            SpisakProdavaca = new List<Korisnik>();
            putanja = HostingEnvironment.MapPath(putanja);
            FileStream stream = new FileStream(putanja, FileMode.Open);
            StreamReader sr = new StreamReader(stream);
            string linija = "";
            while ((linija = sr.ReadLine()) != null)
            {
                string[] parametri = linija.Split(',');
                Pol_korisnika pol_prodavca;
                if (string.Equals(parametri[4], "M")) { pol_prodavca = Pol_korisnika.M; } else { pol_prodavca = Pol_korisnika.Z; }

                Korisnik k = new Korisnik(parametri[0], parametri[1], parametri[2], parametri[3], pol_prodavca,
                                                   DateTime.Parse(parametri[5]), Uloga_korisnika.PRODAVAC, 0, false);
                SpisakProdavaca.Add(k);
            }
            sr.Close();
            stream.Close();
        }

        public void UpisiProdavce()
        {
            string putanja = HostingEnvironment.MapPath("~/App_Data/Prodavci.txt");
            FileStream stream = new FileStream(putanja, FileMode.Create);
            StreamWriter sw = new StreamWriter(stream);
            foreach (Korisnik k in SpisakProdavaca)
            {

                //if (k.Logicki_obrisan == true) { obrisan = "da"; } else { obrisan = "ne"; }
                sw.WriteLine(k.Username + "," + k.Lozinka + "," + k.Ime + "," + k.Prezime + "," + 
                             k.Pol.ToString() + "," + k.Datum_rodjenja.ToShortDateString());
            }
            sw.Close();
            stream.Close();
        }
    }
}