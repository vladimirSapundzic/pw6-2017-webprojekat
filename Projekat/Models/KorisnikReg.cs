﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projekat.Models
{
    public class KorisnikReg
    {
        public KorisnikReg(string username, string lozinka, string ime, string prezime, string datum_rodjenja, string pol)
        {
            Username = username;
            Lozinka = lozinka;
            Ime = ime;
            Prezime = prezime;
            Datum_rodjenja = datum_rodjenja;
            Pol = pol;
        }

        public KorisnikReg()
        {
        }

        public string Username { get; set; }
        public string Lozinka { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }      
        public string Datum_rodjenja { get; set; }
        public string Pol { get; set; }
    }
}