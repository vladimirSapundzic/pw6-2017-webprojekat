﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projekat.Models
{
    public class Tip_korisnika
    {
        public string naziv_tipa;
        public double popust;
        public int br_bodova;

        public Tip_korisnika(string naziv_tipa, double popust, int br_bodova)
        {
            this.naziv_tipa = naziv_tipa;
            this.popust = popust;
            this.br_bodova = br_bodova;
        }
    }
}