﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace Projekat.Models
{


    public class Manifestacije
    {
        public List<Manifestacija> SpisakManifestacija { get; set; }

        public Manifestacije(string putanja)
        {
            SpisakManifestacija = new List<Manifestacija>();
            putanja = HostingEnvironment.MapPath(putanja);
            FileStream stream = new FileStream(putanja, FileMode.Open);
            StreamReader sr = new StreamReader(stream);
            string linija = "";
            while ((linija = sr.ReadLine()) != null)
            {
                string[] parametri = linija.Split(',');
                tip_manifestacije tm;
                status s;

                if (parametri[1].Equals("KONCERT"))
                {
                    tm = tip_manifestacije.KONCERT;
                }
                else if (parametri[2].Equals("FESTIVAL"))
                {
                    tm = tip_manifestacije.FESTIVAL;
                }
                else {
                    tm = tip_manifestacije.POZORISTE;
                }

                if (parametri[5].Equals("AKTIVNO"))
                {
                    s = status.AKTIVNO;
                }
                else if (parametri[5].Equals("NEAKTIVNO"))
                {
                    s = status.NEAKTIVNO;
                }
                else {
                    s = status.NEAKTIVNO;
                }

                MestoOdrzavanja mesto = new MestoOdrzavanja(parametri[6], parametri[7], long.Parse(parametri[8])); 

                Manifestacija m = new Manifestacija(parametri[0], tm, int.Parse(parametri[2]), DateTime.Parse(parametri[3]),
                                                    double.Parse(parametri[4]), s, mesto, parametri[9]);
                SpisakManifestacija.Add(m);
            }
            sr.Close();
            stream.Close();
        }

        public void UpisiManifestacije()
        {
            string putanja = HostingEnvironment.MapPath("~/App_Data/Manifestacije.txt");
            FileStream stream = new FileStream(putanja, FileMode.Create);
            StreamWriter sw = new StreamWriter(stream);
            foreach (Manifestacija m in SpisakManifestacija)
            {
                
                //if (k.Logicki_obrisan == true) { obrisan = "da"; } else { obrisan = "ne"; }

                sw.WriteLine(m.Naziv + "," + m.Tip_man.ToString() + "," + m.Broj_mesta.ToString() + ","
                             + m.Vreme_odr.ToString() + "," + m.Cena_karte
                             + "," + m.St.ToString() + "," + m.MestoOdrzavanja.Ulica + "," + m.MestoOdrzavanja.Mesto 
                             + "," + m.MestoOdrzavanja.Postanski_broj + "," + m.prodavac);
            }
            sw.Close();
            stream.Close();
        }
    }
}