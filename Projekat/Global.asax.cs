﻿using Projekat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Projekat
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            Administratori administratori = new Administratori("~/App_Data/Administratori.txt");
            HttpContext.Current.Application["administratori"] = administratori;
            Prodavci prodavci = new Prodavci("~/App_Data/Prodavci.txt");
            HttpContext.Current.Application["prodavci"] = prodavci;
            Kupci kupci = new Kupci("~/App_Data/Kupci.txt");
            HttpContext.Current.Application["kupci"] = kupci;
            Manifestacije manifestacije = new Manifestacije("~/App_Data/Manifestacije.txt");
            HttpContext.Current.Application["manifestacije"] = manifestacije;
            Komentari komentari = new Komentari("~/App_Data/Komentari.txt");
            HttpContext.Current.Application["komentari"] = komentari;
            RezervisaneKarte karte = new RezervisaneKarte("~/App_Data/RezervisaneKarte.txt");
            HttpContext.Current.Application["rezervisaneKarte"] = karte;

        }
    }
}
