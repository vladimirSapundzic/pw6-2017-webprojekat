﻿using Projekat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Projekat.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            Kupci kupci = (Kupci)HttpContext.Application["kupci"];
            Prodavci prodavci = (Prodavci)HttpContext.Application["prodavci"];
            Administratori administratori = (Administratori)HttpContext.Application["administratori"];
            Manifestacije manifestacije = (Manifestacije)HttpContext.Application["manifestacije"];
            ViewBag.Manifestacije = manifestacije.SpisakManifestacija;
            Session["manifestacije"] = manifestacije.SpisakManifestacija;
            return View();
        }
        public ActionResult Admin()
        {
            Kupci kupci = (Kupci)HttpContext.Application["kupci"];
            Prodavci prodavci = (Prodavci)HttpContext.Application["prodavci"];
            Administratori administratori = (Administratori)HttpContext.Application["administratori"];
            Manifestacije manifestacije = (Manifestacije)HttpContext.Application["manifestacije"];
            ViewBag.Manifestacije = manifestacije.SpisakManifestacija;
            Session["manifestacije"] = manifestacije.SpisakManifestacija;
            ViewBag.Kupci = kupci;
            ViewBag.Prodavci = prodavci;
            ViewBag.KupciFilter = new List<Korisnik>();
            List<Manifestacija> list = new List<Manifestacija>();
            foreach (Manifestacija m in manifestacije.SpisakManifestacija)
            {
                if (m.St == status.NEAKTIVNO)
                {
                    list.Add(m);
                }

            }
            ViewBag.NeodobreneManifestacije = list;

            return View();
        }

        public ActionResult KupacReg()
        {
            

            return View();
        }
        [HttpPost]
        public ActionResult DodajKupca(KorisnikReg param)
        {
            try
            {
                Kupci kupci = (Kupci)HttpContext.Application["kupci"];
                Manifestacije manifestacije = (Manifestacije)HttpContext.Application["manifestacije"];
                ViewBag.Manifestacije = manifestacije.SpisakManifestacija;
                foreach (Korisnik kor in kupci.SpisakKupaca)
                {

                    if (String.Equals(kor.Username, param.Username))
                    {
                        ViewBag.Message = "Korisničko ime je već u upotrebi";
                        return View("Index");
                    }
                }
                Pol_korisnika pol_kupca;
                if (string.Equals(param.Pol, "M")) { pol_kupca = Pol_korisnika.M; } else { pol_kupca = Pol_korisnika.Z; }
                Korisnik k = new Korisnik(param.Username, param.Lozinka, param.Ime, param.Prezime, pol_kupca, DateTime.Parse(param.Datum_rodjenja), Uloga_korisnika.KUPAC, 0, false);
                kupci.SpisakKupaca.Add(k);
                kupci.UpisiKupce();
                ViewBag.Kupac = k;
                Session["Korisnik"] = k;


                return RedirectToAction("Kupac");
            }
            catch (Exception e) {
                ViewBag.Message = "Loš unos podataka";
                return View("KupacReg");
            }
        }
        [HttpPost]
        public ActionResult DodajProdavca(KorisnikReg param)
        {
            try
            {
                Prodavci prodavci = (Prodavci)HttpContext.Application["prodavci"];

                Manifestacije manifestacije = (Manifestacije)HttpContext.Application["manifestacije"];
                ViewBag.Manifestacije = manifestacije.SpisakManifestacija;

                foreach (Korisnik kor in prodavci.SpisakProdavaca)
                {

                    if (String.Equals(kor.Username, param.Username))
                    {
                        ViewBag.Message = "Korisničko ime je već u upotrebi";
                        return View("Admin");
                    }
                }
                Pol_korisnika pol_prodavca;
                if (string.Equals(param.Pol, "M")) { pol_prodavca = Pol_korisnika.M; } else { pol_prodavca = Pol_korisnika.Z; }
                Korisnik k = new Korisnik(param.Username, param.Lozinka, param.Ime, param.Prezime, pol_prodavca, DateTime.Parse(param.Datum_rodjenja), Uloga_korisnika.PRODAVAC, 0, false);
                prodavci.SpisakProdavaca.Add(k);
                prodavci.UpisiProdavce();
                Kupci kupci = (Kupci)HttpContext.Application["kupci"];
                ViewBag.Kupci = kupci;
                ViewBag.Prodavci = prodavci;
            }
            catch (Exception)
            {
                ViewBag.Message = "Greška";
            }
            return RedirectToAction("Admin");
        }

        public ActionResult ProdavacReg()
        {
            
            return View();
        }

        public ActionResult LogOut()
        {
            Session.Clear();

            return RedirectToAction("Index");
        }

        public ActionResult LogIn()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Prijava(LoginParams param)
        {
            Kupci kupci = (Kupci)HttpContext.Application["kupci"];
            Prodavci prodavci = (Prodavci)HttpContext.Application["prodavci"];
            Administratori administratori = (Administratori)HttpContext.Application["administratori"];

            Korisnik kor = (Korisnik)Session["Korisnik"];
            if (kor == null)
            {
                kor = new Korisnik();
                Session["Korisnik"] = kor;
            }

            foreach (Korisnik k in kupci.SpisakKupaca)
            {
                if (k.Lozinka.Equals(param.Lozinka) && k.Username.Equals(param.Username))
                {
                    if (k.Logicki_obrisan == true)
                    {
                        ViewBag.Message = "Korisnik je blokiran od strane administratora";
                        return RedirectToAction("Index");
                    }
                    kor = k;
                    Session["Korisnik"] = kor;
                    return RedirectToAction("Kupac"); // IZMENITI DA PREUSMERI NA PROFIL KUPCA
                }
            }

            foreach (Korisnik k in prodavci.SpisakProdavaca)
            {
                if (k.Lozinka.Equals(param.Lozinka) && k.Username.Equals(param.Username))
                {

                    kor = k;
                    Session["Korisnik"] = kor;


                    return RedirectToAction("Prodavac"); // IZMENITI DA PREUSMERI NA PROFIL PRODAVCA
                }
            }

            foreach (Korisnik k in administratori.SpisakAdministratora)
            {
                if (k.Lozinka.Equals(param.Lozinka) && k.Username.Equals(param.Username))
                {
                    kor = k;
                    Session["Korisnik"] = kor;
                    return RedirectToAction("Admin"); // IZMENITI DA PREUSMERI NA PROFIL ADMINISTRATORA
                }
            }



            ViewBag.Message = "Neispravni podaci";
            return View("LogIn");
        }
        [HttpPost]
        public ActionResult FiltriranjeTip(Filtriranje_TipMan param)
        {
            List<Manifestacija> manifestacije = (List<Manifestacija>)Session["Manifestacije"];
            List<Manifestacija> filter = new List<Manifestacija>();
            tip_manifestacije tm;
            if (string.Equals(param.tip_man, "KONCERT")) { tm = tip_manifestacije.KONCERT; }
            else if (string.Equals(param.tip_man, "POZORISTE")) { tm = tip_manifestacije.POZORISTE; }
            else { tm = tip_manifestacije.FESTIVAL; }
            foreach (Manifestacija m in manifestacije) {
                if (m.Tip_man == tm) {
                    filter.Add(m);
                }
            }
            Session["Manifestacije"] = filter;
            ViewBag.Manifestacije = filter;

            return View("Index");
        }

        [HttpPost]
        public ActionResult FiltriranjeTipKarte(FiltriranjeKarta param)
        {
            List<Karta> manifestacije = (List<Karta>)Session["Karte"];
            List<Karta> filter = new List<Karta>();
            RezervisaneKarte rezervisaneKarte = (RezervisaneKarte)HttpContext.Application["rezervisaneKarte"];
            
            tip_karte tm;
            if (string.Equals(param.tip, "REGULAR")) { tm = tip_karte.REGULAR; }
            else if (string.Equals(param.tip, "VIP")) { tm = tip_karte.VIP; }
            else if(string.Equals(param.tip, "FAN_PIT")) { tm = tip_karte.FAN_PIT; }
            else { tm = tip_karte.REGULAR; }
            foreach (Karta m in manifestacije)
            {
                if (m.Tk == tm)
                {
                    filter.Add(m);
                }
            }
            Session["Karte"] = filter;
            ViewBag.Karte = filter;
            Korisnik k = (Korisnik)Session["Korisnik"];
            ViewBag.Kupac = k;
            Session["Korisnik"] = k;

            List<Rezervacija> rezervacije = new List<Rezervacija>();
            foreach (Rezervacija r in rezervisaneKarte.SpisakRezervacija)
            {
                if (String.Equals(r.Kupac, k.Username))
                {
                    rezervacije.Add(r);
                }
            }

            

            Session["Rezervacije"] = rezervacije;
            ViewBag.Rezervacije = rezervacije;


            return View("Kupac");
        }
        [HttpPost]
        public ActionResult FiltriranjeNerasprodate()
        {
            List<Manifestacija> manifestacije = (List<Manifestacija>)Session["Manifestacije"];
            List<Manifestacija> filter = new List<Manifestacija>();
            
            foreach (Manifestacija m in manifestacije)
            {
                if (m.Broj_mesta > 0)
                {
                    filter.Add(m);
                }
            }
            Session["Manifestacije"] = filter;
            ViewBag.Manifestacije = filter;

            return View("Index");
        }
        [HttpPost]
        public ActionResult Pretraga(string naziv)
        {
            List<Manifestacija> manifestacije = (List<Manifestacija>)Session["Manifestacije"];
            List<Manifestacija> filter = new List<Manifestacija>();
            
            
            foreach (Manifestacija m in manifestacije)
            {
                if (string.Equals(naziv, m.Naziv))
                {
                    filter.Add(m);
                }
            }
            Session["Manifestacije"] = filter;
            ViewBag.Manifestacije = filter;

            return View("Index");
        }
        [HttpPost]
        public ActionResult PretragaMesto(string naziv)
        {
            List<Manifestacija> manifestacije = (List<Manifestacija>)Session["Manifestacije"];
            List<Manifestacija> filter = new List<Manifestacija>();


            foreach (Manifestacija m in manifestacije)
            {
                if (string.Equals(naziv, m.MestoOdrzavanja.Mesto))
                {
                    filter.Add(m);
                }
            }
            Session["Manifestacije"] = filter;
            ViewBag.Manifestacije = filter;

            return View("Index");
        }
        [HttpPost]
        public ActionResult PretragaIme(string ime)
        {
            Kupci kupci = (Kupci)HttpContext.Application["kupci"];
            Prodavci prodavci = (Prodavci)HttpContext.Application["prodavci"];
            Administratori administratori = (Administratori)HttpContext.Application["administratori"];
            Manifestacije manifestacije = (Manifestacije)HttpContext.Application["manifestacije"];
            List<Korisnik> filter = new List<Korisnik>();


            foreach (Korisnik k in kupci.SpisakKupaca)
            {
                if (string.Equals(ime, k.Ime))
                {
                    filter.Add(k);
                }
            }
            ViewBag.Manifestacije = manifestacije.SpisakManifestacija;
            Session["manifestacije"] = manifestacije.SpisakManifestacija;
            ViewBag.Kupci = kupci;
            ViewBag.Prodavci = prodavci;
            List<Manifestacija> list = new List<Manifestacija>();
            foreach (Manifestacija m in manifestacije.SpisakManifestacija)
            {
                if (m.St == status.NEAKTIVNO)
                {
                    list.Add(m);
                }

            }
            ViewBag.NeodobreneManifestacije = list;
            ViewBag.KupciFilter = filter;
            

            return View("Admin");
        }
        [HttpPost]
        public ActionResult PretragaPrezime(string prezime)
        {
            Kupci kupci = (Kupci)HttpContext.Application["kupci"];
            Prodavci prodavci = (Prodavci)HttpContext.Application["prodavci"];
            Administratori administratori = (Administratori)HttpContext.Application["administratori"];
            Manifestacije manifestacije = (Manifestacije)HttpContext.Application["manifestacije"];
            List<Korisnik> filter = new List<Korisnik>();


            foreach (Korisnik k in kupci.SpisakKupaca)
            {
                if (string.Equals(prezime, k.Prezime))
                {
                    filter.Add(k);
                }
            }
            ViewBag.Manifestacije = manifestacije.SpisakManifestacija;
            Session["manifestacije"] = manifestacije.SpisakManifestacija;
            ViewBag.Kupci = kupci;
            ViewBag.Prodavci = prodavci;
            List<Manifestacija> list = new List<Manifestacija>();
            foreach (Manifestacija m in manifestacije.SpisakManifestacija)
            {
                if (m.St == status.NEAKTIVNO)
                {
                    list.Add(m);
                }

            }
            ViewBag.NeodobreneManifestacije = list;
            ViewBag.KupciFilter = filter;


            return View("Admin");
        }
        [HttpPost]
        public ActionResult PretragaUsername(string username)
        {
            Kupci kupci = (Kupci)HttpContext.Application["kupci"];
            Prodavci prodavci = (Prodavci)HttpContext.Application["prodavci"];
            Administratori administratori = (Administratori)HttpContext.Application["administratori"];
            Manifestacije manifestacije = (Manifestacije)HttpContext.Application["manifestacije"];
            List<Korisnik> filter = new List<Korisnik>();


            foreach (Korisnik k in kupci.SpisakKupaca)
            {
                if (string.Equals(username, k.Username))
                {
                    filter.Add(k);
                }
            }
            ViewBag.Manifestacije = manifestacije.SpisakManifestacija;
            Session["manifestacije"] = manifestacije.SpisakManifestacija;
            ViewBag.Kupci = kupci;
            ViewBag.Prodavci = prodavci;
            List<Manifestacija> list = new List<Manifestacija>();
            foreach (Manifestacija m in manifestacije.SpisakManifestacija)
            {
                if (m.St == status.NEAKTIVNO)
                {
                    list.Add(m);
                }

            }
            ViewBag.NeodobreneManifestacije = list;
            ViewBag.KupciFilter = filter;


            return View("Admin");
        }

        [HttpPost]
        public ActionResult TrazenjePoCeni(CenaParam cp)
        {
            List<Manifestacija> manifestacije = (List<Manifestacija>)Session["Manifestacije"];
            List<Manifestacija> filter = new List<Manifestacija>();
            foreach (Manifestacija m in manifestacije)
            {
                if (m.Cena_karte >= cp.donja && m.Cena_karte <= cp.gornja)
                {
                    filter.Add(m);
                }
            }
            Session["Manifestacije"] = filter;
            ViewBag.Manifestacije = filter;

            return View("Index");
        }
        [HttpPost]
        public ActionResult Blokiraj(string un)
        {
            Manifestacije manifestacije = (Manifestacije)HttpContext.Application["manifestacije"];
            Kupci kupci = (Kupci)HttpContext.Application["kupci"];
            Prodavci prodavci = (Prodavci)HttpContext.Application["prodavci"];
            Administratori administratori = (Administratori)HttpContext.Application["administratori"];
            foreach (Korisnik kor in kupci.SpisakKupaca)
            {
                if (String.Equals(kor.Username, un))
                {
                    kor.Logicki_obrisan = true;
                }
            }
            kupci.UpisiKupce();
            ViewBag.Manifestacije = manifestacije.SpisakManifestacija;
            ViewBag.Kupci = kupci;
            ViewBag.Prodavci = prodavci;

            return RedirectToAction("Admin");
        }
        [HttpPost]
        public ActionResult Odobri(string man)
        {
            Manifestacije manifestacije = (Manifestacije)HttpContext.Application["manifestacije"];
            Kupci kupci = (Kupci)HttpContext.Application["kupci"];
            Prodavci prodavci = (Prodavci)HttpContext.Application["prodavci"];
            Administratori administratori = (Administratori)HttpContext.Application["administratori"];
            //int j = pb.id;

            foreach(Manifestacija manif in manifestacije.SpisakManifestacija)
            {
                if(String.Equals(manif.Naziv, man))
                {
                    manif.St = status.AKTIVNO;
                }
            }


            //manifestacije.SpisakManifestacija[j].St = status.AKTIVNO;
            manifestacije.UpisiManifestacije();
            List<Manifestacija> list = new List<Manifestacija>();
            foreach(Manifestacija m in manifestacije.SpisakManifestacija)
            {
                if (m.St == status.NEAKTIVNO) {
                    list.Add(m);
                }

            }
            ViewBag.NeodobreneManifestacije = list;
            ViewBag.Kupci = kupci;
            ViewBag.Prodavci = prodavci;
            

            return RedirectToAction("Admin");
        }
        [HttpPost]
        public ActionResult Odblokiraj(string un)
        {
            Manifestacije manifestacije = (Manifestacije)HttpContext.Application["manifestacije"];
            Kupci kupci = (Kupci)HttpContext.Application["kupci"];
            Prodavci prodavci = (Prodavci)HttpContext.Application["prodavci"];
            Administratori administratori = (Administratori)HttpContext.Application["administratori"];
            //int j = pb.id;
            //kupci.SpisakKupaca[j].Logicki_obrisan = false;
            foreach(Korisnik kor in kupci.SpisakKupaca)
            {
                if(String.Equals(kor.Username, un))
                {
                    kor.Logicki_obrisan = false;
                }
            }


            kupci.UpisiKupce();
            ViewBag.Manifestacije = manifestacije.SpisakManifestacija;
            ViewBag.Kupci = kupci;
            ViewBag.Prodavci = prodavci;


            return RedirectToAction("Admin");
        }

        public static int id_k = 0;

        public ActionResult Kupac()
        {
            Kupci kupci = (Kupci)HttpContext.Application["kupci"];
            Prodavci prodavci = (Prodavci)HttpContext.Application["prodavci"];
            Administratori administratori = (Administratori)HttpContext.Application["administratori"];
            Komentari komentari = (Komentari)HttpContext.Application["komentari"];
            //Administratori administratori = (Administratori)HttpContext.Application["administratori"];
            Manifestacije manifestacije = (Manifestacije)HttpContext.Application["manifestacije"];
            RezervisaneKarte rezervisaneKarte = (RezervisaneKarte)HttpContext.Application["rezervisaneKarte"];
            Korisnik k = (Korisnik)Session["Korisnik"];
            Session["Korisnik"] = k;
            ViewBag.Kupac = k;
            List<Karta> karte = new List<Karta>();
            List<string> ids = new List<string>();

            List<Rezervacija> rezervacije = new List<Rezervacija>();
            foreach (Rezervacija r in rezervisaneKarte.SpisakRezervacija)
            {
                if (String.Equals(r.Kupac, k.Username))
                {
                    rezervacije.Add(r);
                }
                ids.Add(r.Id);
            }

            foreach (Manifestacija m in manifestacije.SpisakManifestacija) {
                if (m.Broj_mesta > 0) {
                    for (int j = 0; j< m.Broj_mesta; j++) {
                        tip_karte tk; double cena;
                        if ((id_k + 1) % 10 == 0) { tk = tip_karte.VIP; cena = m.Cena_karte * 1.5; }
                        else if ((id_k + 1) % 13 == 0) { tk = tip_karte.FAN_PIT; cena = m.Cena_karte * 2; }
                        else { tk = tip_karte.REGULAR; cena = m.Cena_karte; }
                        if (m.St == status.AKTIVNO)
                        {
                            id_k++;
                            if (!ids.Contains(id_k.ToString()))
                            {
                                Karta karta = new Karta((id_k).ToString(), m, m.Vreme_odr, cena, "", tk, status_karte.ODUSTANAK);
                                karte.Add(karta);
                            }
                        }
                    }
                }
            }
            id_k = 0;

            

            Session["Karte"] = karte;
            ViewBag.Karte = karte;

            Session["Rezervacije"] = rezervacije;
            ViewBag.Rezervacije = rezervacije;


            return View();
        }

        public ActionResult Prodavac()
        {
            Kupci kupci = (Kupci)HttpContext.Application["kupci"];
            Prodavci prodavci = (Prodavci)HttpContext.Application["prodavci"];
            Administratori administratori = (Administratori)HttpContext.Application["administratori"];
            Manifestacije manifestacije = (Manifestacije)HttpContext.Application["manifestacije"];
            Komentari komentari = (Komentari)HttpContext.Application["komentari"];
            Korisnik k = (Korisnik)Session["Korisnik"];
            List<Manifestacija> mojeManifestacije = new List<Manifestacija>();
            foreach (Manifestacija m in manifestacije.SpisakManifestacija) {
                if (String.Equals(m.prodavac, k.Username)) {
                    mojeManifestacije.Add(m);
                }
            }
            ViewBag.MojeManifestacije = mojeManifestacije;

            Session["Korisnik"] = k;
            return View();
        }

        public ActionResult ProdavacIzmena()
        {
            Kupci kupci = (Kupci)HttpContext.Application["kupci"];
            Prodavci prodavci = (Prodavci)HttpContext.Application["prodavci"];
            Administratori administratori = (Administratori)HttpContext.Application["administratori"];
            Manifestacije manifestacije = (Manifestacije)HttpContext.Application["manifestacije"];
            Komentari komentari = (Komentari)HttpContext.Application["komentari"];


            ViewBag.Korisnik = (Korisnik)Session["Korisnik"];
            return View();
        }
        [HttpPost]
        public ActionResult Izmeni(KorisnikReg param)
        {
            Prodavci prodavci = (Prodavci)HttpContext.Application["prodavci"];
            Komentari komentari = (Komentari)HttpContext.Application["komentari"];
            int i = 0;
            foreach (Korisnik k in prodavci.SpisakProdavaca) {
                if (String.Equals(param.Username, k.Username)) {
                    k.Lozinka = param.Lozinka;
                    k.Ime = param.Ime;
                    k.Prezime = param.Prezime;
                    k.Datum_rodjenja = DateTime.Parse(param.Datum_rodjenja);
                    if (param.Pol == "M") { k.Pol = Pol_korisnika.M; } else { k.Pol = Pol_korisnika.Z; }
                    prodavci.UpisiProdavce();
                }
                i++;
            }
            return RedirectToAction("Prodavac");
        }

        public ActionResult KupacIzmena()
        {
            Kupci kupci = (Kupci)HttpContext.Application["kupci"];
            Prodavci prodavci = (Prodavci)HttpContext.Application["prodavci"];
            Administratori administratori = (Administratori)HttpContext.Application["administratori"];
            Komentari komentari = (Komentari)HttpContext.Application["komentari"];
            ViewBag.Kupac = (Korisnik)Session["Korisnik"];

            List<Karta> karte = (List<Karta>)Session["Karte"];
            ViewBag.Karte = karte;
            Session["Karte"] = karte;
            return View();
        }
        [HttpPost]
        public ActionResult IzmeniKupca(KorisnikReg param)
        {
            Kupci kupci = (Kupci)HttpContext.Application["kupci"];
            Komentari komentari = (Komentari)HttpContext.Application["komentari"];
            int i = 0;
            foreach (Korisnik k in kupci.SpisakKupaca)
            {
                if (String.Equals(param.Username, k.Username))
                {
                    k.Lozinka = param.Lozinka;
                    k.Ime = param.Ime;
                    k.Prezime = param.Prezime;
                    k.Datum_rodjenja = DateTime.Parse(param.Datum_rodjenja);
                    if (String.Equals(param.Pol, "M")) { k.Pol = Pol_korisnika.M; }
                    else if (String.Equals(param.Pol, "Z")) { k.Pol = Pol_korisnika.Z; }
                    else { k.Pol = Pol_korisnika.M; }
                    kupci.UpisiKupce();
                }
                i++;
            }
            return RedirectToAction("Kupac");
        }

        public ActionResult NovaManifestacija() {
            Kupci kupci = (Kupci)HttpContext.Application["kupci"];
            Prodavci prodavci = (Prodavci)HttpContext.Application["prodavci"];
            Administratori administratori = (Administratori)HttpContext.Application["administratori"];
            Manifestacije manifestacije = (Manifestacije)HttpContext.Application["manifestacije"];
            Komentari komentari = (Komentari)HttpContext.Application["komentari"];
            Korisnik k = (Korisnik)Session["Korsinik"];

            return View();
        }
        [HttpPost]
        public ActionResult DodajManifestaciju(ManifestacijaParam param)
        {
            Prodavci prodavci = (Prodavci)HttpContext.Application["prodavci"];
            Korisnik k = (Korisnik)Session["Korisnik"];
            Manifestacije manifestacije = (Manifestacije)HttpContext.Application["manifestacije"];
            Komentari komentari = (Komentari)HttpContext.Application["komentari"];
            ViewBag.Manifestacije = manifestacije.SpisakManifestacija;

            foreach (Manifestacija man in manifestacije.SpisakManifestacija)
            {

                if (String.Equals(man.Naziv, param.naziv))
                {
                    ViewBag.Message = "Postoji već manifestacija sa istim imenom";
                    return View("NovaManifestacija");
                }
            }
            Session["Korisnik"] = k;
            tip_manifestacije t;

            if(param.mesto == null || param.naziv == null || param.ulica == null || param.tip == null || param.postanski_broj==0|| param.datum_odrzavanja==null) {
                ViewBag.Message = "Neispravni podaci";
                return View("NovaManifestacija");
            }

            if (string.Equals(param.tip, "KONCERT")) { t = tip_manifestacije.KONCERT; }
            else if (string.Equals(param.tip, "POZORIŠTE")) { t = tip_manifestacije.POZORISTE; }
            else { t = tip_manifestacije.FESTIVAL; }
            try
            {
                Manifestacija m = new Manifestacija(param.naziv,
                                                    t,
                                                    param.broj_mesta,
                                                    DateTime.Parse(param.datum_odrzavanja),
                                                    param.cena_karte,
                                                    status.NEAKTIVNO,
                                                    new MestoOdrzavanja(param.ulica, param.mesto, param.postanski_broj),
                                                    k.Username);
                manifestacije.SpisakManifestacija.Add(m);
                manifestacije.UpisiManifestacije();

                return RedirectToAction("Prodavac");
            }
            catch (Exception) {
                ViewBag.Message = "Neispravni podaci";
                return View("NovaManifestacija"); }
        }
        [HttpPost]
        public ActionResult SortiranjePoCeni(SortParam sp)
        {
            Manifestacije manifestacije = (Manifestacije)HttpContext.Application["manifestacije"];
            Komentari komentari = (Komentari)HttpContext.Application["komentari"];
            if (String.Equals(sp.sortTip, "RASTUĆE"))
            {
                for (int i = 0; i < manifestacije.SpisakManifestacija.Count; i++)
                {
                    for (int j = i + 1; j < manifestacije.SpisakManifestacija.Count; j++)
                    {
                        if (manifestacije.SpisakManifestacija[i].Cena_karte > manifestacije.SpisakManifestacija[j].Cena_karte)
                        {
                            Manifestacija temp = new Manifestacija();
                            temp = manifestacije.SpisakManifestacija[i];
                            manifestacije.SpisakManifestacija[i] = manifestacije.SpisakManifestacija[j];
                            manifestacije.SpisakManifestacija[j] = temp;
                        }
                    }
                }
            }
            else
            {
                for (int i = 0; i < manifestacije.SpisakManifestacija.Count; i++)
                {
                    for (int j = i + 1; j < manifestacije.SpisakManifestacija.Count; j++)
                    {
                        if (manifestacije.SpisakManifestacija[i].Cena_karte < manifestacije.SpisakManifestacija[j].Cena_karte)
                        {
                            Manifestacija temp = new Manifestacija();
                            temp = manifestacije.SpisakManifestacija[i];
                            manifestacije.SpisakManifestacija[i] = manifestacije.SpisakManifestacija[j];
                            manifestacije.SpisakManifestacija[j] = temp;
                        }
                    }
                }
            }
            
           manifestacije.UpisiManifestacije();
            List<Manifestacija> lista = manifestacije.SpisakManifestacija;
            Session["Manifestacije"] = lista;
            ViewBag.Manifestacije = lista;
            return View("Index");
        }
        [HttpPost]
        public ActionResult SortiranjePoDatumu(SortParam sp)
        {
            Manifestacije manifestacije = (Manifestacije)HttpContext.Application["manifestacije"];
            Komentari komentari = (Komentari)HttpContext.Application["komentari"];
            if (String.Equals(sp.sortTip, "RASTUĆE"))
            {
                for (int i = 0; i < manifestacije.SpisakManifestacija.Count; i++)
                {
                    for (int j = i + 1; j < manifestacije.SpisakManifestacija.Count; j++)
                    {
                        if (manifestacije.SpisakManifestacija[i].Vreme_odr > manifestacije.SpisakManifestacija[j].Vreme_odr)
                        {
                            Manifestacija temp = new Manifestacija();
                            temp = manifestacije.SpisakManifestacija[i];
                            manifestacije.SpisakManifestacija[i] = manifestacije.SpisakManifestacija[j];
                            manifestacije.SpisakManifestacija[j] = temp;
                        }
                    }
                }
            }
            else
            {
                for (int i = 0; i < manifestacije.SpisakManifestacija.Count; i++)
                {
                    for (int j = i + 1; j < manifestacije.SpisakManifestacija.Count; j++)
                    {
                        if (manifestacije.SpisakManifestacija[i].Vreme_odr < manifestacije.SpisakManifestacija[j].Vreme_odr)
                        {
                            Manifestacija temp = new Manifestacija();
                            temp = manifestacije.SpisakManifestacija[i];
                            manifestacije.SpisakManifestacija[i] = manifestacije.SpisakManifestacija[j];
                            manifestacije.SpisakManifestacija[j] = temp;
                        }
                    }
                }
            }

            manifestacije.UpisiManifestacije();
            List<Manifestacija> lista = manifestacije.SpisakManifestacija;
            Session["Manifestacije"] = lista;
            ViewBag.Manifestacije = lista;
            return View("Index");
        }
        [HttpPost]
        public ActionResult SortiranjePoMestu(SortParam sp)
        {
            Manifestacije manifestacije = (Manifestacije)HttpContext.Application["manifestacije"];
            Komentari komentari = (Komentari)HttpContext.Application["komentari"];
            if (String.Equals(sp.sortTip, "RASTUĆE"))
            {
                for (int i = 0; i < manifestacije.SpisakManifestacija.Count; i++)
                {
                    for (int j = i + 1; j < manifestacije.SpisakManifestacija.Count; j++)
                    {
                        if (manifestacije.SpisakManifestacija[i].MestoOdrzavanja.Mesto.ToCharArray()[0] > manifestacije.SpisakManifestacija[j].MestoOdrzavanja.Mesto.ToCharArray()[0])
                        {
                            Manifestacija temp = new Manifestacija();
                            temp = manifestacije.SpisakManifestacija[i];
                            manifestacije.SpisakManifestacija[i] = manifestacije.SpisakManifestacija[j];
                            manifestacije.SpisakManifestacija[j] = temp;
                        }
                    }
                }
            }
            else
            {
                for (int i = 0; i < manifestacije.SpisakManifestacija.Count; i++)
                {
                    for (int j = i + 1; j < manifestacije.SpisakManifestacija.Count; j++)
                    {
                        if (manifestacije.SpisakManifestacija[i].MestoOdrzavanja.Mesto.ToCharArray()[0] < manifestacije.SpisakManifestacija[j].MestoOdrzavanja.Mesto.ToCharArray()[0])
                        {
                            Manifestacija temp = new Manifestacija();
                            temp = manifestacije.SpisakManifestacija[i];
                            manifestacije.SpisakManifestacija[i] = manifestacije.SpisakManifestacija[j];
                            manifestacije.SpisakManifestacija[j] = temp;
                        }
                    }
                }
            }

            manifestacije.UpisiManifestacije();
            List<Manifestacija> lista = manifestacije.SpisakManifestacija;
            Session["Manifestacije"] = lista;
            ViewBag.Manifestacije = lista;
            return View("Index");
        }
        [HttpPost]
        public ActionResult SortiranjePoNazivu(SortParam sp)
        {
            Manifestacije manifestacije = (Manifestacije)HttpContext.Application["manifestacije"];
            Komentari komentari = (Komentari)HttpContext.Application["komentari"];
            if (String.Equals(sp.sortTip, "RASTUĆE"))
            {
                for (int i = 0; i < manifestacije.SpisakManifestacija.Count; i++)
                {
                    for (int j = i + 1; j < manifestacije.SpisakManifestacija.Count; j++)
                    {
                        if (manifestacije.SpisakManifestacija[i].Naziv.ToCharArray()[0] > manifestacije.SpisakManifestacija[j].Naziv.ToCharArray()[0])
                        {
                            Manifestacija temp = new Manifestacija();
                            temp = manifestacije.SpisakManifestacija[i];
                            manifestacije.SpisakManifestacija[i] = manifestacije.SpisakManifestacija[j];
                            manifestacije.SpisakManifestacija[j] = temp;
                        }
                    }
                }
            }
            else
            {
                for (int i = 0; i < manifestacije.SpisakManifestacija.Count; i++)
                {
                    for (int j = i + 1; j < manifestacije.SpisakManifestacija.Count; j++)
                    {
                        if (manifestacije.SpisakManifestacija[i].Naziv.ToCharArray()[0] < manifestacije.SpisakManifestacija[j].Naziv.ToCharArray()[0])
                        {
                            Manifestacija temp = new Manifestacija();
                            temp = manifestacije.SpisakManifestacija[i];
                            manifestacije.SpisakManifestacija[i] = manifestacije.SpisakManifestacija[j];
                            manifestacije.SpisakManifestacija[j] = temp;
                        }
                    }
                }
            }

            manifestacije.UpisiManifestacije();
            List<Manifestacija> lista = manifestacije.SpisakManifestacija;
            Session["Manifestacije"] = lista;
            ViewBag.Manifestacije = lista;
            return View("Index");
        }
        [HttpPost]
        public ActionResult SortiranjePoImenu(SortParam sp)
        {
            Kupci kupci = (Kupci)HttpContext.Application["kupci"];
            Komentari komentari = (Komentari)HttpContext.Application["komentari"];
            if (String.Equals(sp.sortTip, "RASTUĆE"))
            {
                for (int i = 0; i < kupci.SpisakKupaca.Count; i++)
                {
                    for (int j = i + 1; j < kupci.SpisakKupaca.Count; j++)
                    {
                        if (kupci.SpisakKupaca[i].Ime.ToCharArray()[0] > kupci.SpisakKupaca[j].Ime.ToCharArray()[0])
                        {
                            Korisnik temp = new Korisnik();
                            temp = kupci.SpisakKupaca[i];
                            kupci.SpisakKupaca[i] = kupci.SpisakKupaca[j];
                            kupci.SpisakKupaca[j] = temp;
                        }
                    }
                }
            }
            else
            {
                for (int i = 0; i < kupci.SpisakKupaca.Count; i++)
                {
                    for (int j = i + 1; j < kupci.SpisakKupaca.Count; j++)
                    {
                        if (kupci.SpisakKupaca[i].Ime.ToCharArray()[0] < kupci.SpisakKupaca[j].Ime.ToCharArray()[0])
                        {
                            Korisnik temp = new Korisnik();
                            temp = kupci.SpisakKupaca[i];
                            kupci.SpisakKupaca[i] = kupci.SpisakKupaca[j];
                            kupci.SpisakKupaca[j] = temp;
                        }
                    }
                }
            }

            kupci.UpisiKupce();
            List<Korisnik> lista = kupci.SpisakKupaca;
            Session["Kupci"] = lista;
            ViewBag.Kupci = lista;
            return RedirectToAction("Admin");
        }
        [HttpPost]
        public ActionResult SortiranjePoPrezimenu(SortParam sp)
        {
            Kupci kupci = (Kupci)HttpContext.Application["kupci"];
            Komentari komentari = (Komentari)HttpContext.Application["komentari"];
            if (String.Equals(sp.sortTip, "RASTUĆE"))
            {
                for (int i = 0; i < kupci.SpisakKupaca.Count; i++)
                {
                    for (int j = i + 1; j < kupci.SpisakKupaca.Count; j++)
                    {
                        if (kupci.SpisakKupaca[i].Prezime.ToCharArray()[0] > kupci.SpisakKupaca[j].Prezime.ToCharArray()[0])
                        {
                            Korisnik temp = new Korisnik();
                            temp = kupci.SpisakKupaca[i];
                            kupci.SpisakKupaca[i] = kupci.SpisakKupaca[j];
                            kupci.SpisakKupaca[j] = temp;
                        }
                    }
                }
            }
            else
            {
                for (int i = 0; i < kupci.SpisakKupaca.Count; i++)
                {
                    for (int j = i + 1; j < kupci.SpisakKupaca.Count; j++)
                    {
                        if (kupci.SpisakKupaca[i].Prezime.ToCharArray()[0] < kupci.SpisakKupaca[j].Prezime.ToCharArray()[0])
                        {
                            Korisnik temp = new Korisnik();
                            temp = kupci.SpisakKupaca[i];
                            kupci.SpisakKupaca[i] = kupci.SpisakKupaca[j];
                            kupci.SpisakKupaca[j] = temp;
                        }
                    }
                }
            }

            kupci.UpisiKupce();
            List<Korisnik> lista = kupci.SpisakKupaca;
            Session["Kupci"] = lista;
            ViewBag.Kupci = lista;
            return RedirectToAction("Admin");
        }
        [HttpPost]
        public ActionResult SortiranjeUsername(SortParam sp)
        {
            Kupci kupci = (Kupci)HttpContext.Application["kupci"];
            Komentari komentari = (Komentari)HttpContext.Application["komentari"];
            if (String.Equals(sp.sortTip, "RASTUĆE"))
            {
                for (int i = 0; i < kupci.SpisakKupaca.Count; i++)
                {
                    for (int j = i + 1; j < kupci.SpisakKupaca.Count; j++)
                    {
                        if (kupci.SpisakKupaca[i].Username.ToCharArray()[0] > kupci.SpisakKupaca[j].Username.ToCharArray()[0])
                        {
                            Korisnik temp = new Korisnik();
                            temp = kupci.SpisakKupaca[i];
                            kupci.SpisakKupaca[i] = kupci.SpisakKupaca[j];
                            kupci.SpisakKupaca[j] = temp;
                        }
                    }
                }
            }
            else
            {
                for (int i = 0; i < kupci.SpisakKupaca.Count; i++)
                {
                    for (int j = i + 1; j < kupci.SpisakKupaca.Count; j++)
                    {
                        if (kupci.SpisakKupaca[i].Username.ToCharArray()[0] < kupci.SpisakKupaca[j].Username.ToCharArray()[0])
                        {
                            Korisnik temp = new Korisnik();
                            temp = kupci.SpisakKupaca[i];
                            kupci.SpisakKupaca[i] = kupci.SpisakKupaca[j];
                            kupci.SpisakKupaca[j] = temp;
                        }
                    }
                }
            }

            kupci.UpisiKupce();
            List<Korisnik> lista = kupci.SpisakKupaca;
            Session["Kupci"] = lista;
            ViewBag.Kupci = lista;
            return RedirectToAction("Admin");
        }

        public ActionResult KombinovanaPretraga()
        {
            Komentari komentari = (Komentari)HttpContext.Application["komentari"];
            Korisnik k = (Korisnik)Session["Korisnik"];
            Session["Korisnik"] = k;
            List<Karta> karte = (List<Karta>)Session["Karte"];
            Session["Karte"] = karte;
            ViewBag.Karte = karte;
            return View();
        }

        public ActionResult PretraziKomb(KombinovanaPretraga param)
        {
            Korisnik k = (Korisnik)Session["Korisnik"];
            Komentari komentari = (Komentari)HttpContext.Application["komentari"];
            RezervisaneKarte rezervisaneKarte = (RezervisaneKarte)HttpContext.Application["rezervisaneKarte"];
            Session["Korisnik"] = k;
            ViewBag.Kupac = k;
            List<Karta> karte = (List<Karta>)Session["Karte"];
            List<Karta> filter = new List<Karta>();
            tip_manifestacije t;
            if (string.Equals(param.tip_man, "KONCERT")) { t = tip_manifestacije.KONCERT; }
            else if (string.Equals(param.tip_man, "POZORIŠTE")) { t = tip_manifestacije.POZORISTE; }
            else { t = tip_manifestacije.FESTIVAL; }

            tip_karte tk;
            if (string.Equals(param.tip_kar, "REGULAR")) { tk = tip_karte.REGULAR; }
            else if (string.Equals(param.tip_kar, "VIP")) { tk = tip_karte.VIP; }
            else if (string.Equals(param.tip_kar, "FAN_PIT")){ tk = tip_karte.FAN_PIT; }
            else
            {
                tk = tip_karte.REGULAR;
            }

            if(param.datum_do == null)
            {
                param.datum_do = "31.12.2099.";
            }
            if (param.datum_od == null) {
                param.datum_od = "1.1.1.";
            }
            DateTime datumOd = DateTime.MinValue;
            if (!DateTime.TryParse(param.datum_od, out datumOd)){ datumOd = DateTime.MinValue; }
            DateTime datumDo = DateTime.MaxValue;
            if(!DateTime.TryParse(param.datum_do, out datumDo)) { datumDo = DateTime.MaxValue; }


            foreach (Karta karta in karte) {
                if(karta.Tk == tk)
                {
                    if(karta.Manifestacija.Tip_man == t)
                    {
                        if (karta.Cena <= param.cena_do && karta.Cena >= param.cena_od) {
                            if (karta.DateTime <= datumDo && karta.DateTime >= datumOd){
                                filter.Add(karta);
                            }
                        }
                    }
                }
            }

            List<Rezervacija> rezervacije = new List<Rezervacija>();
            foreach (Rezervacija r in rezervisaneKarte.SpisakRezervacija)
            {
                if (String.Equals(r.Kupac, k.Username))
                {
                    rezervacije.Add(r);
                }
            }

            Session["Rezervacije"] = rezervacije;
            ViewBag.Rezervacije = rezervacije;

            Session["Karte"] = filter;
            ViewBag.Karte = filter;
            return View("Kupac");
        }

        public ActionResult PregledMan (string ime_man)
        {
            Kupci kupci = (Kupci)HttpContext.Application["kupci"];
            Prodavci prodavci = (Prodavci)HttpContext.Application["prodavci"];
            Administratori administratori = (Administratori)HttpContext.Application["administratori"];
            Manifestacije manifestacije = (Manifestacije)HttpContext.Application["manifestacije"];
            Komentari komentari = (Komentari)HttpContext.Application["komentari"];
            Manifestacija result = new Manifestacija();

            ViewBag.Korisnik = (Korisnik)Session["Korisnik"];
           

            List<Komentar> comments = new List<Komentar>();

            foreach (Manifestacija m in manifestacije.SpisakManifestacija) {
                if (String.Equals(m.Naziv, ime_man)) {
                    result = m;
                }
            }
            foreach (Komentar k in komentari.SpisakKomentara)
            {
                if (String.Equals(k.Manifestacija, ime_man))
                {
                    comments.Add(k);
                }
            }

            result.Naziv = ime_man;

            ViewBag.Manifestacija = result;
            Session["Manifestacija"] = result;
            ViewBag.Komentari = comments;
            Session["Komentari"] = comments;

            return View("PregledManifestacije");
        }

        public ActionResult KomentarDodavanje()
        {
            Kupci kupci = (Kupci)HttpContext.Application["kupci"];
            Prodavci prodavci = (Prodavci)HttpContext.Application["prodavci"];
            Administratori administratori = (Administratori)HttpContext.Application["administratori"];
            Manifestacije manifestacije = (Manifestacije)HttpContext.Application["manifestacije"];
            Komentari komentari = (Komentari)HttpContext.Application["komentari"];
            Manifestacija result = new Manifestacija();
            result = (Manifestacija)Session["Manifestacija"];
            Session["Manifestacija"] = result;
            Korisnik k = (Korisnik)Session["Korisnik"];
            ViewBag.Korisnik = (Korisnik)Session["Korisnik"];
            Session["Korisnik"] = k;

            return View();
        }

        public ActionResult DodajKom(KomentarParam kp)
        {
            Kupci kupci = (Kupci)HttpContext.Application["kupci"];
            Prodavci prodavci = (Prodavci)HttpContext.Application["prodavci"];
            Administratori administratori = (Administratori)HttpContext.Application["administratori"];
            Manifestacije manifestacije = (Manifestacije)HttpContext.Application["manifestacije"];
            Komentari komentari = (Komentari)HttpContext.Application["komentari"];
            Manifestacija result = new Manifestacija();


            Korisnik k = (Korisnik)Session["Korisnik"];
            Manifestacija m = (Manifestacija)Session["Manifestacija"];

            Komentar kom = new Komentar(k.Username, m.Naziv, kp.tekst, int.Parse(kp.ocena));
            komentari.SpisakKomentara.Add(kom);
            komentari.UpisiKomentare();


            ViewBag.Korisnik = (Korisnik)Session["Korisnik"];
            Session["Korisnik"] = k;
            Session["MAnifestacija"] = m;

            return RedirectToAction("Kupac");
        }

        public ActionResult Rezervacija(string id, string ime_man)
        {
            Kupci kupci = (Kupci)HttpContext.Application["kupci"];
            Prodavci prodavci = (Prodavci)HttpContext.Application["prodavci"];
            Administratori administratori = (Administratori)HttpContext.Application["administratori"];
            Manifestacije manifestacije = (Manifestacije)HttpContext.Application["manifestacije"];
            Komentari komentari = (Komentari)HttpContext.Application["komentari"];
            RezervisaneKarte rezervisaneKarte = (RezervisaneKarte)HttpContext.Application["rezervisaneKarte"];
            Manifestacija result = new Manifestacija();


            Korisnik k = (Korisnik)Session["Korisnik"];

            foreach (Manifestacija m in manifestacije.SpisakManifestacija)
            {
                if (String.Equals(m.Naziv, ime_man))
                {
                    result = m;
                }
            }



            rezervisaneKarte.SpisakRezervacija.Add(new Rezervacija(id, ime_man, k.Username));
            rezervisaneKarte.UpisiRezervacije();

            k.Bodovi += (int)result.Cena_karte / 3;

            foreach (Korisnik kor in kupci.SpisakKupaca)
            {
                if (String.Equals(k.Username, kor.Username))
                {
                    kor.Bodovi += (int)result.Cena_karte / 3;
                }
            }
            kupci.UpisiKupce();

            ViewBag.Korisnik = (Korisnik)Session["Korisnik"];
            Session["Korisnik"] = k;
            

            return RedirectToAction("Kupac");
        }

        public ActionResult Otkazivanje(string id, string ime_man)
        {
            Kupci kupci = (Kupci)HttpContext.Application["kupci"];
            Prodavci prodavci = (Prodavci)HttpContext.Application["prodavci"];
            Administratori administratori = (Administratori)HttpContext.Application["administratori"];
            Manifestacije manifestacije = (Manifestacije)HttpContext.Application["manifestacije"];
            Komentari komentari = (Komentari)HttpContext.Application["komentari"];
            RezervisaneKarte rezervisaneKarte = (RezervisaneKarte)HttpContext.Application["rezervisaneKarte"];
            Manifestacija result = new Manifestacija();


            Korisnik k = (Korisnik)Session["Korisnik"];

            foreach (Manifestacija m in manifestacije.SpisakManifestacija)
            {
                if (String.Equals(m.Naziv, ime_man))
                {
                    result = m;
                }
            }

            foreach (Rezervacija m in rezervisaneKarte.SpisakRezervacija)
            {
                if (String.Equals(m.Id, id))
                {
                    rezervisaneKarte.SpisakRezervacija.Remove(m);
                    break;
                }
            }



            rezervisaneKarte.UpisiRezervacije();

            k.Bodovi -= (int)result.Cena_karte;

            foreach (Korisnik kor in kupci.SpisakKupaca)
            {
                if (String.Equals(k.Username, kor.Username))
                {
                    kor.Bodovi -= (int)result.Cena_karte / 3;
                }
            }
            kupci.UpisiKupce();

            ViewBag.Korisnik = (Korisnik)Session["Korisnik"];
            Session["Korisnik"] = k;


            return RedirectToAction("Kupac");
        }

    }
}